import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { DetailComponent } from './detail/detail.component';
import { HomeComponent } from './home/home.component';
import { CartComponent } from './cart/cart.component';
import { ShippingComponent } from './shipping/shipping.component';
import { TransactionComponent } from './transaction/transaction.component';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { PurchaseComponent } from './purchase/purchase.component';
import { ProfileComponent } from './profile/profile.component';
import { AddressComponent } from './address/address.component';
import { ConfirmationComponent } from './confirmation/confirmation.component';

const appRoutes : Routes = [
  {
    path:'detail',
    component: DetailComponent,
    data: { title: 'Detail' }
  },
  {
    path:'',
    redirectTo:'/home',
    pathMatch:'full'
  },
  {
    path:'home',
    component:HomeComponent,
    data: { title: 'Home' }
  },
  {
    path:'cart',
    component:CartComponent,
    data: { title: 'Cart' }
  },
  {
    path:'shipping',
    component:ShippingComponent,
    data: { title: 'Shipping' }
  },
  {
    path:'transaction',
    component:TransactionComponent,
    data: { title: 'Transaction' }
  },
  {
    path:'register',
    component:RegisterComponent,
    data: { title: 'Register' }
  },
  {
    path:'login',
    component:LoginComponent,
    data: { title: 'login' }
  },
  {
    path:'confirmation',
    component:ConfirmationComponent,
    data: { title: 'confirmation' }
  },
  {
    path:'member/purchase',
    component:PurchaseComponent,
    data: { title: 'purchase' }
  },
  {
    path:'member/profile',
    component:ProfileComponent,
    data: { title: 'profile' }
  },
  {
    path:'member/address',
    component:AddressComponent,
    data: { title: 'address' }
  },
]


@NgModule({
  declarations: [
    AppComponent,
    DetailComponent,
    HomeComponent,
    CartComponent,
    ShippingComponent,
    TransactionComponent,
    RegisterComponent,
    LoginComponent,
    PurchaseComponent,
    ProfileComponent,
    AddressComponent,
    ConfirmationComponent
  ],
  imports: [
    BrowserModule,
    NgbModule.forRoot(),
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true }
    ),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
